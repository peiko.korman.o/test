<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\UrlSlugDetailsDTO;
use App\Models\ShortUrl;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ShortUrlService
{
    public function createShortUrl(string $url): string
    {
        $model = $this->createModel($url);
        $salt = $this->getBase($model->id) . $model->salt;

        return route('shortUrl', ['slug' => $salt]);
    }

    public function getUrlFromSlug(string $slug): string
    {
        $urlSlugDetails = $this->decodeUrlSlug($slug);
        if (!$urlSlugDetails){
            throw new NotFoundHttpException();
        }
        /** @var ShortUrl $model */
        $model = ShortUrl::query()
            ->where('id', $urlSlugDetails->id)
            ->where('salt', $urlSlugDetails->salt)
            ->firstOrFail();

        return $model->url;
    }

    private function getRandomSymbol(): string
    {
        return $this->getBase(rand(0, 35), 36);
    }

    private function getBase(int $number, int $toBase = 32): string
    {
        return base_convert((string)$number, 10, $toBase);
    }

    private function createModel(string $url): ShortUrl
    {
        $model = new ShortUrl();
        $model->url = $url;
        $model->salt = $this->getRandomSymbol() . $this->getRandomSymbol();
        $model->save();

        return $model;
    }

    private function decodeUrlSlug(string $slug): ?UrlSlugDetailsDTO
    {
        if (strlen($slug) >= 3) {
            return new UrlSlugDetailsDTO([
                'salt' => substr($slug, -2),
                'id' => (int) base_convert(substr($slug, 0, -2), 32, 10)
            ]);
        }

        return null;
    }
}
