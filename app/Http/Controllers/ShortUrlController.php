<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateShortUrlRequest;
use App\Services\ShortUrlService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class ShortUrlController extends Controller
{
    public function __construct(private ShortUrlService $shortUrlService)
    {
    }

    public function redirect(string $slug): Redirector|Application|RedirectResponse
    {
        return redirect($this->shortUrlService->getUrlFromSlug($slug));
    }

    public function store(CreateShortUrlRequest $request): Response|Application|ResponseFactory
    {
        return response($this->shortUrlService->createShortUrl($request->url));
    }

    public function createForm(): Factory|View|Application
    {
        return view('shortUrl.form');
    }
}
