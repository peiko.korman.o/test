<?php

declare(strict_types=1);

namespace App\DTO;

class BaseDTO
{
    public function __construct(array $config = [])
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}
