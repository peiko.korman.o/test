<?php

declare(strict_types=1);

namespace App\DTO;

class UrlSlugDetailsDTO extends BaseDTO
{
    public int $id;
    public string $salt;
}
