DOCKER_COMPOSE = docker-compose -f ./docker/docker-compose.yml
DOCKER_COMPOSE_PHP_FPM_EXEC = ${DOCKER_COMPOSE} exec -u www-data short-php-fpm

up:
	${DOCKER_COMPOSE} up -d

down:
	${DOCKER_COMPOSE} down --remove-orphans

build:
	${DOCKER_COMPOSE} build

dc_ps:
	${DOCKER_COMPOSE} ps

dc_logs:
	${DOCKER_COMPOSE} logs -f

php:
	docker exec -it short-php-fpm bash

init:
	cp .env.example .env
	cp ./docker/.env.dist ./docker/.env
	make up
	docker exec -it short-php-fpm composer install
	docker exec -it short-php-fpm php artisan migrate
	docker exec -it short-php-fpm php artisan key:generate
