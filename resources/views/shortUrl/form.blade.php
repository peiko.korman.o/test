<div id="app">
    <div class="container">
        <h3 class="text-center">Create short url</h3>
        <div class="row">
            <div class="col-md-6">
                <form @submit.prevent="create">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Url</label>
                        <input class="form-control" v-model="model.url">
                        <small v-if="errors.url" class="form-text text-muted">@{{ errors.url }}</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Create Short Url</button>
                </form>
                <a v-if="result" v-bind:href="result" target="_blank">@{{ result }}</a>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"
        integrity="sha512-odNmoc1XJy5x1TMVMdC7EMs3IVdItLPlCeL5vSUPN2llYKMJ2eByTTAIiiuqLg+GdNr9hF6z81p27DArRFKT7A=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    var app = new Vue({
        el: '#app',
        data: {
            model: {},
            errors: {
                url: null
            },
            result: null
        },
        methods: {
            create() {

                axios
                    .post('http://localhost/api/short', this.model)
                    .then(response => (
                        this.result = response.data
                    ))
                    .catch(error => {
                        if (error.response.status === 422) {
                            Object.keys(this.errors).forEach((field) => {
                                if (error.response.data.errors[field]) {
                                    this.errors[field] = error.response.data.errors[field][0]
                                } else {
                                    this.errors[field] = null
                                }
                            });
                        }
                    })
            }
        }
    })
</script>
