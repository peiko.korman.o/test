<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('short/{slug}', [\App\Http\Controllers\ShortUrlController::class, 'redirect'])->name('shortUrl');
Route::post('short', [\App\Http\Controllers\ShortUrlController::class, 'store'])->name('createShortUrl');
