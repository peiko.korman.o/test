<?php

declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShortUrlTest extends TestCase
{
    use RefreshDatabase;

    public function test_url_redirection()
    {
        $url = 'https://google.com/';

        $response = $this->postJson(route('createShortUrl'), ['url' => $url]);
        $responseUrl = $response->getContent();

        $this->get($responseUrl)->assertRedirect($url);
    }
}
